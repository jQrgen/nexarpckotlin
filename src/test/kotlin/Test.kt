package Nexa.NexaRpc

import java.math.BigDecimal
import kotlin.test.Test
import kotlin.test.*

class NexaRpcTest
{
    @Test
    fun basicReg()
    {
        println("This test requires that a nexa regtest network be running on the localhost at port 18332")
        val rpc = NexaRpcFactory.create("http://127.0.0.1:18332")

        // clear anything already there
        val blkhashes2 = rpc.generate(1)
        assertEquals(blkhashes2.size, 1)

        val unspent = rpc.listunspent()
        assertTrue {unspent.size > 0 }
        assertEquals(1 ,1)

        val bal = rpc.getbalance()
        assertTrue {bal > BigDecimal.ZERO }

        val peers = rpc.getpeerinfo()
        assertTrue{peers.size < 2}  // Probably true anyway

        val addr = rpc.getnewaddress()
        println(addr)
        val addr2 = rpc.getnewaddress("p2pkh")
        println(addr2)
        assertTrue { addr.length > addr2.length}  // just a property of the 2 address formats that helps confirm that the right ones were created
        val tx = rpc.sendtoaddress(addr, BigDecimal("1000.23"))

        val txpool = rpc.getrawtxpool()
        assertEquals(txpool.size, 1)

        val txpoolinfo = rpc.gettxpoolinfo()
        assertEquals(txpoolinfo.size, 1)
        assertTrue { txpoolinfo.bytes > 100}

        val rawTx = rpc.getrawtransaction(txpool[0].toHex())
        assertTrue { rawTx.size > 100 }

        val txi = rpc.gettransaction(txpool[0].toHex())
        println(txi.toString())
        assertEquals(txi.confirmations,0L)
        assertEquals(txi.generated, false)

        rpc.abandontransaction(txpool[0])

        val txpool2 = rpc.getrawtxpool()
        assertEquals(txpool2.size, 0)


        val height = rpc.getblockcount()
        val blkhashes = rpc.generate(1)
        assertEquals(blkhashes.size, 1)
        val height2 = rpc.getblockcount()
        assertEquals(height+1, height2)
        rpc.invalidateblock(blkhashes[0])
        val height3 = rpc.getblockcount()
        assertEquals(height, height3)

        val blk = rpc.getblock(0)
        assertEquals(blk.height, 0)
        assertEquals(blk.hash, HashId("d71ee431e307d12dfef31a6b21e071f1d5652c0eb6155c04e3222612c9d0b371"))
    }
}
