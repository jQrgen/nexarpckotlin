package Nexa.NexaRpc

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.coroutines.*
import java.util.*
import kotlinx.serialization.json.*
import java.math.BigDecimal
//import java.util.logging.Logger

//private val LogIt = Logger.getLogger("Nexa.NexaRpc")

object NexaRpcFactory
{
    //actual
    fun create(url:String="http://127.0.0.1:18332/", user:String ="regtest", pwd:String = "regtest"):NexaRpc = JvmNexaRpc(url, user, pwd)
}

open class JvmNexaRpc(override val url:String="http://127.0.0.1:18332/", override val user:String ="regtest", override val pwd:String = "regtest"): NexaRpc
{
    var reqCount = 0
    val encoder: Base64.Encoder = Base64.getEncoder()
    val authString = "Basic " + encoder.encodeToString((user + ":" + pwd).toByteArray())
    val json = kotlinx.serialization.json.Json { encodeDefaults = true; ignoreUnknownKeys = true }

    fun formatJsonRpcRequest(method:String, params:List<String>?=null):String
    {
        val s = StringBuilder()
        val pString = params?.map({ "\"" + it + "\"" })?.joinToString(separator = ",") ?: ""
        s.append("""{ "method" :"${method}", """)
        s.append(""" "params" : [ ${pString} ], """)
        s.append(""" "id" : ${reqCount} }""")
        reqCount += 1
        return s.toString()
    }

    //private val DB = Dispatchers.IO.limitedParallelism(2)
    suspend fun _calls(rpcName: String, params:List<String>?=null): String
    {
        val client = HttpClient(CIO)
        try
        {
            val response: HttpResponse = client.post(url)
            {
                headers {
                    append(HttpHeaders.Connection, "close")
                    append(HttpHeaders.Authorization, authString)
                }
                setBody(formatJsonRpcRequest(rpcName, params))
            }
            //println(response.status)
            val respBody: String = response.body()
            // println(respBody)
            return respBody
        }
        catch (e: Exception)
        {
            println(e.toString())
            throw(e)
        }
        finally
        {
            client.close()
        }
    }

    suspend fun _callje(rpcName: String, params:List<String>?=null): JsonElement = json.parseToJsonElement(_calls(rpcName, params))


    suspend fun _listunspent(): List<NexaRpc.Unspent>
    {
        val resp = _calls("listunspent")
        val ret = json.decodeFromString(NexaRpc.ListUnspentReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _generate(qty: Int):List<HashId>
    {
        val resp = _calls("generate", listOf(qty.toString()))
        val ret = json.decodeFromString(NexaRpc.HexHashListReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result.map() { HashId(it) }
    }

    suspend fun _getrawtxpool():List<HashId>
    {
        val resp = _calls("getrawtxpool")
        val ret = json.decodeFromString(NexaRpc.HexHashListReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result.map() { HashId(it) }
    }

    suspend fun _sendtoaddress(addr: String, amt: BigDecimal):HashId
    {
        val resp = _calls("sendtoaddress", listOf(addr, amt.toString()))
        val decode = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (decode.result == null)
            throw NexaRpcException(decode.error?.message ?: "Unspecified Error", decode.error?.code ?: 0)
        return HashId(decode.result)
    }

    suspend fun _getbalance(): BigDecimal
    {
        val resp = _calls("getbalance")
        val ret = json.decodeFromString(NexaRpc.GetBalanceReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return BigDecimal(ret.result)
    }

    suspend fun _getblockcount(): Long
    {
        val resp = _calls("getblockcount")
        val ret = json.decodeFromString(NexaRpc.LongReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _evicttransaction(txid: String): Long
    {
        val resp = _calls("evicttransaction", listOf(txid))
        val ret = json.decodeFromString(NexaRpc.LongReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _invalidateblock(id: String)
    {
        val resp = _calls("invalidateblock", listOf(id))
        val ret = json.decodeFromString(NexaRpc.NothingReply.serializer(), resp)
        if (ret.error != null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
    }

    suspend fun _abandontransaction(id: String)
    {
        val resp = _calls("abandontransaction", listOf(id))
        val ret = json.decodeFromString(NexaRpc.NothingReply.serializer(), resp)
        if (ret.error != null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
    }

    suspend fun _getwalletinfo(): NexaRpc.WalletInfo
    {
        val resp = _calls("getwalletinfo")
        val ret = json.decodeFromString(NexaRpc.GetWalletInfoReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _getblock(which:String): NexaRpc.BlockInfo
    {
        val resp = _calls("getblock", listOf(which))
        val ret = json.decodeFromString(NexaRpc.GetBlockReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _getpeerinfo(): List<NexaRpc.PeerInfo>
    {
        val resp = _calls("getpeerinfo")
        //LogIt.info(" getpeerinfo: " + resp)
        val ret = json.decodeFromString(NexaRpc.GetPeerInfoReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _getnewaddress(addrType:String?): String
    {
        val resp = _calls("getnewaddress", if (addrType != null) listOf(addrType) else null)
        val ret = json.decodeFromString(NexaRpc.GetNewAddressReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _getrawtransaction(txhash:String): ByteArray
    {
        val resp = _calls("getrawtransaction", listOf(txhash))
        val ret = json.decodeFromString(NexaRpc.HexHashReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result.fromHex()
    }

    suspend fun _gettransaction(txhash: String): NexaRpc.TransactionInfo
    {
        val resp = _calls("gettransaction", listOf(txhash))
        val ret = json.decodeFromString(NexaRpc.GetTransactionReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    suspend fun _gettxpoolinfo(): NexaRpc.TxPoolInfo
    {
        val resp = _calls("gettxpoolinfo")
        val ret = json.decodeFromString(NexaRpc.TxPoolInfoReply.serializer(), resp)
        if (ret.result == null)
            throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
        return ret.result
    }

    val peerInfo: List<NexaRpc.PeerInfo>
        get() = getpeerinfo()


    fun _bch_getmempoolinfo():NexaRpc.BchMemPoolInfo
    {
        var result: NexaRpc.BchMemPoolInfo? = null
        runBlocking {
            val resp = _calls("getmempoolinfo")
            val ret:NexaRpc.BchMemPoolInfoReply = json.decodeFromString(NexaRpc.BchMemPoolInfoReply.serializer(), resp)
            if (ret.result == null)
                throw NexaRpcException(ret.error?.message ?: "Unspecified Error", ret.error?.code ?: 0)
            result = ret.result
        }
        return result!!
    }

    override fun getpeerinfo() = runBlocking { _getpeerinfo() }

    override fun listunspent() = runBlocking { _listunspent() }

    override fun generate(qty: Int) = runBlocking { _generate(qty) }
    override fun getrawtxpool() = runBlocking { _getrawtxpool() }
    override fun sendtoaddress(addr: String, amt: BigDecimal) = runBlocking { _sendtoaddress(addr, amt) }
    override fun getbalance() = runBlocking { _getbalance() }
    override fun getwalletinfo() = runBlocking { _getwalletinfo() }
    override fun calls(rpcName: String, params:List<String>?): String = runBlocking { _calls(rpcName, params) }
    override fun callje(rpcName: String, params:List<String>?): JsonElement = runBlocking { _callje(rpcName, params) }
    override fun getnewaddress(addrType:String?) = runBlocking { _getnewaddress(addrType)}
    override fun gettxpoolinfo() = runBlocking { _gettxpoolinfo() }

    override fun getrawtransaction(hash:String): ByteArray = runBlocking { _getrawtransaction(hash)}
    override fun getrawtransaction(hash:HashId): ByteArray = getrawtransaction(hash.toHex())

    override fun gettransaction(hash:String) = runBlocking { _gettransaction(hash)}

    override fun getblock(hash:HashId): NexaRpc.BlockInfo = runBlocking {_getblock(hash.toString())}
    override fun getblock(height:Long): NexaRpc.BlockInfo = runBlocking {_getblock(height.toString())}

    override fun getblockcount(): Long = runBlocking { _getblockcount() }

    override fun evicttransaction(hash: String)  = runBlocking { _evicttransaction(hash) }

    override fun invalidateblock(hash: String)  = runBlocking { _invalidateblock(hash) }

    override fun abandontransaction(hash: String)  = runBlocking { _abandontransaction(hash) }

    override fun bch_getmempoolinfo() = runBlocking { _bch_getmempoolinfo() }
}
