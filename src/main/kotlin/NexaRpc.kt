package Nexa.NexaRpc

import kotlinx.serialization.*
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonElement
import java.math.BigDecimal

class NexaRpcException(msg:String, val code: Long):Exception(msg)

private val HEX_CHARS = "0123456789abcdef"
// Convert a hex string to a ByteArray
fun String.fromHex(): ByteArray
{

    val result = ByteArray(length / 2)

    for (i in 0 until length step 2)
    {
        val firstIndex = HEX_CHARS.indexOf(this[i]);
        val secondIndex = HEX_CHARS.indexOf(this[i + 1]);

        val octet = firstIndex.shl(4).or(secondIndex)
        result.set(i.shr(1), octet.toByte())
    }

    return result
}

fun Byte.toUint():Int = toInt() and 0xFF

fun ByteArray.toHex(): String
{
    val ret = StringBuilder()
    for (b in this)
    {
        ret.append(HEX_CHARS[b.toUint() shr 4])
        ret.append(HEX_CHARS[b.toUint() and 0xf])
    }
    return ret.toString()
}

@Serializable(with = HashIdAsStringSerializer::class)
class HashId(val hash: ByteArray = ByteArray(32, { _ -> 0 }))
{
    constructor(hex: String) : this()
    {
        val hsh = hex.fromHex()
        hsh.reverse()
        hsh.copyInto(hash)
    }

    /** Convert to the bitcoin standard hex representation */
    fun toHex(): String
    {
        val cpy = hash.copyOf()
        cpy.reverse()
        return cpy.toHex()
    }

    /** The default display will be bitcoin standard hex representation (reversed hex) */
    override fun toString(): String = this.toHex()

    override fun equals(other: Any?): Boolean
    {
        if (other is HashId) return hash contentEquals other.hash
        return false
    }
}

object HashIdAsStringSerializer: KSerializer<HashId>
{
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("HashId", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: HashId)
    {
        val string = value.toHex()
        encoder.encodeString(string)
    }

    override fun deserialize(decoder: Decoder): HashId
    {
        val string = decoder.decodeString()
        return HashId(string)
    }
}

interface NexaRpc
{
    @Serializable
    data class RpcError(val code:Long, val message: String)

    @Serializable
    // Do not use the amount field because it is not a decimal fraction.  Use the satoshis field.
    data class Unspent(val outpoint: String, val txid: String, val txidem: String, val vout: Long, val address:String, val scriptPubKey: String, val scriptType: String, val satoshi: Long, val amount: Double, val confirmations: Long, val spendable: Boolean)
    @Serializable
    data class ListUnspentReply(val result: List<Unspent>?, val error: RpcError?)

    @Serializable
    data class GetBalanceReply(val result: Double?, val error: RpcError?)  // TODO change to BigDecimal when supported by Kotlin

    @Serializable
    data class WalletInfo(val walletversion: Long, val syncblock: String, val syncheight:Long, val Balance: Double, val unconfirmed_balance: Double, val immature_balance: Double, val txcount: Long, val keypoololdest: Long, val keypoolsize:Long, val paytxfee: Long, val hdmasterkeyid:String)
    @Serializable
    data class GetWalletInfoReply(val result: WalletInfo?, val error: RpcError?)

    @Serializable
    data class GetNewAddressReply(val result: String?, val error: RpcError?)

    @Serializable
    data class TxPoolInfo(val size: Long, val bytes: Long, val usage: Long, val maxtxpool: Long, val txpoolminfee: Double, val tps: Double, val peak_tps: Double)
    @Serializable
    data class TxPoolInfoReply (val result: TxPoolInfo?, val error: RpcError?)

    @Serializable
    data class PeerInfo(val id:Long, val addr: String, val addrlocal: String, val services: String, val servicesnames: List<String>, val relaytxes: Boolean, val lastsend: Long, val lastrecv: Long,
                        val bytessent: Long, val bytesrecv:Long, val conntime: Long, val timeoffset: Long, val pingtime:Double, val minping: Double, val version: Long, val subver: String, val inbound: Boolean,
                        val startingheight: Long, val banscore: Long, val synced_headers: Long, val synced_blocks: Long, val inflight: List<String>, val whitelisted: Boolean, val extversion_map: Map<String, String>)
    @Serializable
    data class GetPeerInfoReply(val result: List<PeerInfo>, val error: RpcError?)

    @Serializable data class BlockInfo(val hash: HashId, val confirmations: Long, val height: Long, val size: Long, val feePoolAmt: Long, val merkleroot:HashId, val time: Long, val mediantime: Long,
                                       val nonce: String, val bits: String, val difficulty: Double, val chainwork: String, val utxoCommitment: String, val minerData: String, val ancestorhash: HashId, val nextblockhash: HashId? = null,
                                       val txid: List<HashId>, val txidem: List<HashId>)
    @Serializable
    data class GetBlockReply(val result: BlockInfo?, val error: RpcError?)

    @Serializable
    data class LongReply(val result: Long?, val error: RpcError?)

    @Serializable
    data class NothingReply(val error: RpcError?)


    @Serializable
    data class HexHashReply(val result: String?, val error: RpcError?)
    @Serializable
    data class HexHashListReply(val result: List<String>?, val error: RpcError?)

    @Serializable data class TxDetail(val account: String, val address: String, val category: String, val satoshi: Long, val amount: Double, val vout: Long)
    @Serializable data class TransactionInfo(val satoshi: Long, val amount: Double, val confirmations: Long, val generated: Boolean = false, val blockhash: HashId? = null, val blockindex: Long?=null, val blocktime: Long?=null,
    val txid: HashId, val txidem: HashId, val walletconflicts: List<HashId>, val time: Long, val timereceived: Long, val details: List<TxDetail>, val hex: String)
    @Serializable
    data class GetTransactionReply(val result: TransactionInfo?, val error: RpcError?)


    /// Some BCH/Bitcoin RPCs are also added for ease of interoperability:
    @Serializable
    data class BchMemPoolInfo(val size: Long, val bytes: Long, val usage: Long, val maxmempool: Long, val mempoolminfee: Double, val tps: Double, val peak_tps: Double)
    @Serializable
    data class BchMemPoolInfoReply (val result: BchMemPoolInfo?, val error: RpcError?)


    val url:String?
    val user:String?
    val pwd:String?

    fun callje(rpcName: String, params:List<String>?=null): JsonElement
    fun calls(rpcName: String, params:List<String>?=null): String

    fun listunspent(): List<Unspent>
    fun generate(qty: Int):List<HashId>
    fun getrawtxpool():List<HashId>
    fun gettxpoolinfo(): TxPoolInfo
    fun sendtoaddress(addr: String, amt: BigDecimal):HashId
    fun getbalance(): BigDecimal
    fun getwalletinfo(): WalletInfo
    fun getpeerinfo(): List<PeerInfo>
    fun getnewaddress(addrType:String? = null): String

    fun getrawtransaction(hash:String): ByteArray
    fun getrawtransaction(hash:HashId): ByteArray

    fun gettransaction(hash:String): TransactionInfo
    fun gettransaction(hash:HashId) = gettransaction(hash.toHex())

    fun getblock(hash:HashId): BlockInfo
    fun getblock(height:Long): BlockInfo

    fun getblockcount(): Long
    fun evicttransaction(hash: HashId) = evicttransaction(hash.toHex())
    fun evicttransaction(hash: String): Long
    fun invalidateblock(hash: HashId) = invalidateblock(hash.toHex())
    fun invalidateblock(hash: String)

    fun abandontransaction(hash: HashId) = abandontransaction(hash.toHex())
    fun abandontransaction(hash: String)

    // Some BCH specific functions are implemented for ease of interop testing
    fun bch_getmempoolinfo(): BchMemPoolInfo
}

/*
expect object NexaRpcFactory
{
    fun create(url:String="http://10.0.2.2:18332/", user:String ="regtest", pwd:String = "regtest"): NexaRpc
}

 */


