# NexaRpcKotlin

This is a Kotlin library to access the Nexa full node via RPC commands.  These are the same commands as are available via "nexa-cli", but it happens programmatically.  This library is very useful when writing automated tests for other projects.

## Access
Note the the full node RPC commands are only available to the node owners, so you will need to run your own full node.  Additionally, if running the graphical full node, you must include the line "server=1" in your nexa.conf file.  If you can access the full node via "nexa-cli", you ought to be able to access it via this library.  For more details, see the full node documentation.

## Command Documentation

Documentation of the RPC commands is available in the full node.  For example, run "nexa-cli help" to see all commands and "nexa-cli help getwalletinfo" to see help for the getwalletinfo command.


## Installation

Look here https://gitlab.com/nexa/nexarpckotlin/-/packages for the latest package version.  Let's say its "9.9.9".
In the application level build.gradle.kts include the repository:
```
repositories {
    maven { url = uri("https://gitlab.com/api/v4/projects/38119368/packages/maven") }
}
```

Then include the library as a dependency:

```
dependencies {
implementation("Nexa","NexaRpc","9.9.9")
}
```

or if you are using this library in your test code:

```
testImplementation("Nexa","NexaRpc","9.9.9")
```

## Example

See the Test.kt file in this repository

- [ ] [Set up project integrations](https://gitlab.com/nexa/nexarpckotlin/-/settings/integrations)

## Usage

Create an access object using the following code:

```
import Nexa.NexaRpc.*


fun whatever()
{
    val usernameFromNexaConf = "regtest"
    val passwordFromNexaConf = "regtest"

    val rpcConnection = "http://127.0.0.1:18332"
    val nexaRpc = NexaRpcFactory.create(rpcConnection, usernameFromNexaConf, passwordFromNexaConf)
    
    // Now you can access it like this
    val ret = nexaRpc.getwalletinfo()
    
}
```


If a function call has not been provided for an RPC, you can call it manually via 2 APIs: calls, which returns a string, and callje which return a JsonElement.  You will need to do your own parsing.  Or add the function call yourself and open an MR!  Its easy, see the existing code for examples!

```
   val ret = nexaRpc.calls("getchaintips")
   println(ret)
   val ret = nexaRpc.callje("getchaintips")
```
